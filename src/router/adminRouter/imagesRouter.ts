import express from 'express';
import ImagesController from '../../controllers/admin/ImagesController';

const imagesController = ImagesController.instance;

const imagesRouter = express.Router();
imagesRouter.get('/list', imagesController.list);
imagesRouter.get('/status', imagesController.status);
imagesRouter.delete('/remove', imagesController.remove);
imagesRouter.patch('/rename/:imageFileName', imagesController.rename);

export default imagesRouter;
