import express from 'express';
import UploadController from '../../controllers/admin/UploadController';

const uploadController = UploadController.instance;

const uploadRouter = express.Router();
uploadRouter.post('/', uploadController.upload, uploadController.afterUpload);

export default uploadRouter;
