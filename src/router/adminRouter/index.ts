import express from 'express';
import imagesRouter from './imagesRouter';
import uploadRouter from './uploadRouter';

const adminRouter = express.Router();
adminRouter.use('/upload', uploadRouter);
adminRouter.use('/images', imagesRouter);

export default adminRouter;
