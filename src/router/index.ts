import bodyParser from 'body-parser';
import express, { Application, Request, Response } from 'express';
import adminRouter from './adminRouter';

import fs from 'node:fs';
import path from 'node:path';
import accessControlAllow from '../middleware/accessControlAllow';
import authenticateToken from '../middleware/authenticateToken';
import ENV from '../core/ENV';

const router = (app: Application) => {
    //
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    //
    app.use(accessControlAllow);

    app.use('/public', express.static(ENV.PUBLIC_DIR));

    //
    app.use('/admin', authenticateToken, adminRouter);
    app.use('/', (req: Request, res: Response) => {
        const data = fs.readFileSync(path.join(__dirname, '../../static/upload.html'), 'utf-8').toString();

        res.status(200).send(data);
        res.end();
        return;
    });
};

export default router;
