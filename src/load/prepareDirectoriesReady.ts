import fs from 'node:fs';
import path from 'node:path';
import ENV from '../core/ENV';
import lapisLog from '../core/lapisLog';

const prepareDirectory = async (path: string): Promise<boolean> => {
    return new Promise((resolve, reject) => {
        if (fs.existsSync(path)) {
            resolve(true);
            return;
        }

        try {
            fs.mkdir(path, { recursive: true }, (error) => {
                if (error) {
                    lapisLog('ERROR', `Can't make dir with path:"${path}". Error message: "${error.message}"`);
                    resolve(false);
                    return;
                }
                resolve(true);
            });
        } catch (e) {
            reject('An unknown error has occurred');
        }
    });
};

const prepareDirectoriesReady = async () => {
    try {
        const results = await Promise.all([
            prepareDirectory(ENV.STATIC_DIR),
            prepareDirectory(ENV.UPLOAD_DIR),
            prepareDirectory(ENV.IMAGES_LARGE_DIR),
            prepareDirectory(ENV.IMAGES_MEDIUM_DIR),
            prepareDirectory(ENV.IMAGES_SMALL_DIR),
            prepareDirectory(ENV.IMAGES_THUMBNAIL_DIR),
        ]);

        const final = results.every((item) => item === true);

        if (final) lapisLog('SUCCESS', `All directories are ready`);
        else lapisLog('SUCCESS', `Some directories do not exists and failure to create`);

        return final;
    } catch (e) {
        lapisLog('ERROR', e);
        return false;
    }
};

export default prepareDirectoriesReady;
