import { NextFunction, Request, Response } from 'express';
import multer from 'multer';
import path from 'path';
import api from '../../api';
import ENV from '../../core/ENV';
import lapisLog from '../../core/lapisLog';
import IFormConvert from '../../core/types/TFormConvert';
import multerUpload from '../../middleware/multerUpload';
import fs from 'node:fs';
import ImagesCache from '../../cache/ImagesCache';
import removeVietnameseAccents from '../../functions/removeVietnameseAccents';
import make from '../../functions/make';
import ProcessImages from '../../functions/ProcessImages';
import allowFileNameExtension from '../../config/allowFileNameExtension';

class UploadController {
    private static _instance?: UploadController;

    public static get instance() {
        if (!this._instance) this._instance = new UploadController();
        return this._instance;
    }

    protected constructor() {}

    public index = async (req: Request, res: Response) => {};

    public upload = async (req: Request, res: Response, next: NextFunction) => {
        multerUpload.array('images', 50)(req, res, (err) => {
            if (err instanceof multer.MulterError) {
                res.status(400).send(`Có một lỗi xảy ra trong quá trình upload: ${err.message}; code: ${err.code}`);
                res.end();
                return;
            }
            if (err) {
                res.status(400).send(
                    `Có một lỗi không xác định xảy ra trong quá trình upload: ${err.message} code: ${err.code}`,
                );
                res.end();
                return;
            }
            next();
        });
    };

    public afterUpload = async (req: Request, res: Response, next: NextFunction) => {
        if (!req.files || !Array.isArray(req.files) || req.files.length === 0) {
            res.status(200).send(
                `Không có file nào được tải lên. Có thể bạn đã tải lên những file không hợp lệ. File hợp lệ bao gồm các file có phần mở rộng: ${allowFileNameExtension.join(
                    ', ',
                )}`,
            );
            return;
        }

        req.files.forEach((item) => {
            ProcessImages.instance.push({
                path: item.path,
                originalname: item.originalname,
            });
        });

        res.status(201).send();
    };
}

export default UploadController;
