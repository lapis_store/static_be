import { Request, Response } from 'express';
import ImagesCache from '../../cache/ImagesCache';
import lapisLog from '../../core/lapisLog';
import ProcessImages from '../../functions/ProcessImages';
import removeImages from '../../functions/removeImages';
import renameImages from '../../functions/renameImages';

class ImagesController {
    private static _instance?: ImagesController;
    public static get instance() {
        if (!this._instance) this._instance = new ImagesController();
        return this._instance;
    }

    protected constructor() {}

    public list = async (req: Request, res: Response) => {
        try {
            const images = await ImagesCache.instance.getImages();
            res.status(200).json(images);
            return res.end();
        } catch {
            res.status(500).send();
            return res.end();
        }
    };

    public status = async (req: Request, res: Response) => {
        res.status(200).json({
            lastUpdate: ImagesCache.instance.lastUpdate,
            processing: ProcessImages.instance.length,
        });
    };

    public remove = async (req: Request, res: Response) => {
        const imageName = req.query.imageName;
        if (!imageName || typeof imageName !== 'string') {
            res.status(400).send();
            return res.end();
        }

        const result = await removeImages(imageName);
        ImagesCache.instance.isNewest = false;

        if (result) {
            res.status(201).json({
                status: 'successfully',
            });
            return res.end();
        }

        res.status(500).send();
        return res.end();
    };

    public rename = async (req: Request, res: Response) => {
        const imageFileName = req.params.imageFileName;
        const newImageName = req.body.newImageName;

        if (typeof newImageName !== 'string') {
            return res.status(400).send();
        }

        const reg = /^[a-z0-9\s]*$/i;
        if (!reg.test(newImageName)) {
            return res.status(400).send('File name chỉ chứa các ký tự tù a-z, 0-9');
        }

        const images = await ImagesCache.instance.getImages();
        if (!images.includes(imageFileName)) {
            return res.status(404).send();
        }

        // den0f_181bb71125600002_1x1.webp
        const tmp = imageFileName.split('_');
        if (tmp.length !== 3) {
            return res.status(500).send();
        }

        let newImageFileName = [
            newImageName.replace(/\s/g, '-'),
            tmp[1], // id
            tmp[2], //
        ].join('_');

        await renameImages(imageFileName, newImageFileName);

        ImagesCache.instance.isNewest = false;

        res.status(200).send();
    };
}

export default ImagesController;
