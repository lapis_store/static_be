import multer from 'multer';
import allowFileNameExtension from '../config/allowFileNameExtension';
import ENV from '../core/ENV';
import lapisLog from '../core/lapisLog';

// const storage = multer.diskStorage({
//     destination(req, file, callback) {

//     },
//     filename(req, file, callback) {

//     },
// })

const multerUpload = multer({
    dest: ENV.UPLOAD_DIR,
    limits: {
        fileSize: 100 * 1024 * 1024, // 100MB
    },
    fileFilter(req, file, callback) {
        const extension = file.originalname.split('.').at(-1);

        if (!extension || !allowFileNameExtension.includes(extension)) {
            lapisLog('WARNING', `An invalid file "${file.originalname}" was rejected`);
            callback(null, false);
            return;
        }

        callback(null, true);
    },
});

export default multerUpload;
