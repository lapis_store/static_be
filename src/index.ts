import './core/configEnv';

import express from 'express';

import lapisLog from './core/lapisLog';
import prepareDirectoriesReady from './load/prepareDirectoriesReady';
import router from './router';
import ENV from './core/ENV';

(async () => {
    const results = await Promise.all([prepareDirectoriesReady()]);

    if (results.includes(false)) {
        lapisLog('INFO', 'Some process error');
        return;
    }

    const app = express();

    router(app);

    app.listen(ENV.PORT, () => {
        lapisLog('INFO', `Server running on port: ${ENV.PORT}`);
    });
})();
