import ENV from '../core/ENV';
import lapisLog from '../core/lapisLog';
import listAllFiles from '../functions/listAllFiles';

class ImagesCache {
    private static _instance?: ImagesCache;
    private _images?: string[];
    private _isNewest: boolean = false;
    private _lastUpdate: number = Date.now();

    public static get instance() {
        if (!this._instance) this._instance = new ImagesCache();
        return this._instance;
    }

    public get isNewest() {
        return this._isNewest;
    }

    public set isNewest(v: boolean) {
        this._lastUpdate = Date.now();
        this._isNewest = v;
    }

    public get lastUpdate() {
        return this._lastUpdate;
    }

    public getImages = async () => {
        if (!this._images || !this.isNewest) {
            this._images = await listAllFiles(ENV.IMAGES_THUMBNAIL_DIR);
            this.isNewest = true;
            this._lastUpdate = Date.now();
            lapisLog('INFO', `Upload ImagesCache`);
        }

        return this._images;
    };

    protected constructor() {}
}

export default ImagesCache;
