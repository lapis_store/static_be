import convert from './convert';

const api = {
    convert,
};

export default api;
