import axios from 'axios';
import lapisLog from '../core/lapisLog';
import IFormConvert, { IConvertResponse } from '../core/types/TFormConvert';

const convert = async (form: IFormConvert) => {
    try {
        const res = await axios.post('http://127.0.0.1:3003/convert', {
            ...form,
        });

        const data: IConvertResponse = res.data;
        return {
            status: res.status,
            data,
        };
    } catch (e) {
        lapisLog('ERROR', e);
        return undefined;
    }
};

export default convert;
