export interface IFormConvertAction {
    width: number;
    save: string;
    lossless: boolean;
    quality: number;
    thumbnail: boolean;
}

interface IFormConvert {
    path: string;
    actions: IFormConvertAction[];
}

export default IFormConvert;

export interface ISize {
    width: number;
    height: number;
}

export interface IConvertDataItem {
    path: string;
    size: ISize;
}

export interface IConvertResponse {
    status: 'successfully' | 'failure';
    size: ISize;
    message: string;
    data: IConvertDataItem[];
}
