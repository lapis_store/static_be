import { Request } from 'express';

export interface ITokenPayload {
    userId: string;
}

export interface TWithToken {
    tokenPayload: ITokenPayload;
}

export type WithTokenNotReadonly<T> = TWithToken & T;

type WithToken<T> = Readonly<TWithToken> & T;
export default WithToken;
