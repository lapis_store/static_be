import path from 'path';

class ENV {
    public static readonly STATIC_DIR: string = (() => {
        if (process.env.STATIC_DIR) {
            return process.env.STATIC_DIR;
        }

        // default
        return path.join(__dirname, '../../static');
    })();

    public static readonly UPLOAD_DIR: string = (() => {
        return path.join(this.STATIC_DIR, '/uploads');
    })();

    public static readonly PUBLIC_DIR: string = (() => {
        return path.join(this.STATIC_DIR, '/public');
    })();

    public static readonly IMAGES_LARGE_DIR: string = (() => {
        return path.join(this.PUBLIC_DIR, '/images/large');
    })();

    public static readonly IMAGES_MEDIUM_DIR: string = (() => {
        return path.join(this.PUBLIC_DIR, '/images/medium');
    })();

    public static readonly IMAGES_SMALL_DIR: string = (() => {
        return path.join(this.PUBLIC_DIR, '/images/small');
    })();

    public static readonly IMAGES_THUMBNAIL_DIR: string = (() => {
        return path.join(this.PUBLIC_DIR, '/images/thumbnail');
    })();

    public static readonly PORT: number = (() => {
        if (process.env.PORT) {
            const port = parseInt(process.env.PORT);
            if (isFinite(port) && port > 3000) {
                return port;
            }
        }

        // default
        return 3004;
    })();

    public static readonly ACCESS_TOKEN_SECRET: string = process.env.ACCESS_TOKEN_SECRET || '1245145aa';
}

export default ENV;
