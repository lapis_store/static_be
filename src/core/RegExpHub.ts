class RegExpHub {
    public static phoneNumber = /^[0-9]{10}$/i;
    public static objectId = /^[0-9a-f]{24}$/i;
    public static date = /^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}Z$/i;
    public static integer = /^\d*$/i;
}

export default RegExpHub;
