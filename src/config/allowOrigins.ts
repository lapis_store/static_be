export interface TAllowOrigins {
    protocol: ('http' | 'https')[];
    domain: string;
    ports: number[];
}

const allowOrigins: TAllowOrigins[] = [
    {
        protocol: ['http', 'https'],
        domain: 'lapisstore.lapisblog.com',
        ports: [80, 443],
    },
    {
        protocol: ['http', 'https'],
        domain: 'dev-lapisstore.lapisblog.com',
        ports: [80, 443],
    },
    {
        protocol: ['http', 'https'],
        domain: 'admin-lapisstore.lapisblog.com',
        ports: [80, 443],
    },
    {
        protocol: ['http', 'https'],
        domain: 'dev-admin-lapisstore.lapisblog.com',
        ports: [80, 443],
    },
];

export default allowOrigins;
