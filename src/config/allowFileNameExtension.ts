const allowFileNameExtension: string[] = ['jpg', 'jpeg', 'png', 'webp', 'gif', 'raw'];

export default allowFileNameExtension;
