import fs from 'node:fs';
import path from 'node:path';
import ENV from '../core/ENV';
import lapisLog from '../core/lapisLog';

export const renameFile = async (dirName: string, oldName: string, newName: string) => {
    const oldPath = path.join(dirName, oldName);
    const newPath = path.join(dirName, newName);

    if (oldPath === newPath) return true;

    if (!fs.existsSync(oldPath)) return false;

    try {
        fs.renameSync(oldPath, newPath);
        return true;
    } catch {
        return false;
    }
};

const renameImages = async (oldImageFileName: string, newImageFileName: string) => {
    return !(
        await Promise.all([
            renameFile(ENV.IMAGES_LARGE_DIR, oldImageFileName, newImageFileName),
            renameFile(ENV.IMAGES_MEDIUM_DIR, oldImageFileName, newImageFileName),
            renameFile(ENV.IMAGES_SMALL_DIR, oldImageFileName, newImageFileName),
            renameFile(ENV.IMAGES_THUMBNAIL_DIR, oldImageFileName, newImageFileName),
        ])
    ).includes(false);
};

export default renameImages;
