import fs from 'node:fs';
import lapisLog from '../core/lapisLog';

const listAllFiles = async (path: string): Promise<string[]> => {
    return new Promise((resolve, reject) => {
        try {
            fs.readdir(path, (err, files) => {
                if (err) {
                    lapisLog('ERROR', `listAllFiles: ${err.message}, code: ${err.code}`);
                    return;
                }

                resolve([...files]);
            });
        } catch {
            reject(`An unknown error when readdir "${path}"`);
        }
    });
};

export default listAllFiles;
