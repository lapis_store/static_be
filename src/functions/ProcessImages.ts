import path from 'path';
import fs from 'node:fs';

import api from '../api';
import ENV from '../core/ENV';
import IFormConvert from '../core/types/TFormConvert';
import make from './make';
import removeVietnameseAccents from './removeVietnameseAccents';
import ImagesCache from '../cache/ImagesCache';
import lapisLog from '../core/lapisLog';

export interface IProcessImage {
    path: string;
    originalname: string;
}

class ProcessImages {
    private static _instance?: ProcessImages;
    private _data: IProcessImage[] = [];
    private isProcessing: boolean = false;

    public static get instance() {
        if (!this._instance) this._instance = new ProcessImages();
        return this._instance;
    }

    public get length() {
        if (this.isProcessing) {
            return this._data.length + 1;
        }
        return this._data.length;
    }

    protected constructor() {}

    private makeFormConvert = (imgPath: string, originalName: string) => {
        let fileName = originalName.split('.')[0];
        fileName = fileName.trim();
        fileName = removeVietnameseAccents(fileName);
        fileName = fileName.replace(/[_\s]/, '-');
        fileName = fileName.replace(/[^a-zA-Z0-9]/g, '0');

        fileName = `${fileName}_${make.id()}`;
        console.log(fileName);

        const convertConf: IFormConvert = {
            path: imgPath,
            actions: [
                {
                    width: 1300,
                    save: path.join(ENV.IMAGES_LARGE_DIR, fileName),
                    lossless: false,
                    quality: 80,
                    thumbnail: false,
                },
                {
                    width: 1000,
                    save: path.join(ENV.IMAGES_MEDIUM_DIR, fileName),
                    lossless: false,
                    quality: 80,
                    thumbnail: false,
                },
                {
                    width: 600,
                    save: path.join(ENV.IMAGES_SMALL_DIR, fileName),
                    lossless: false,
                    quality: 80,
                    thumbnail: false,
                },
                {
                    width: 200,
                    save: path.join(ENV.IMAGES_THUMBNAIL_DIR, fileName),
                    lossless: false,
                    quality: 80,
                    thumbnail: true,
                },
            ],
        };
        return convertConf;
    };

    private process = async () => {
        this.isProcessing = true;
        while (this._data.length > 0) {
            const item = this._data.shift();
            if (!item) break;

            const config = this.makeFormConvert(item.path, item.originalname);

            try {
                const convertRes = await api.convert(config);

                if (!convertRes || convertRes.status !== 201) {
                    lapisLog('ERROR', `Convert "${item.originalname}" failure !`);
                }
            } catch {
                lapisLog('ERROR', `Convert "${item.originalname}" failure !`);
            }

            ImagesCache.instance.isNewest = false;
            fs.unlinkSync(item.path); // remove file after convert success
        }

        this.isProcessing = false;
    };

    public push = (v: IProcessImage) => {
        this._data.push(v);

        if (!this.isProcessing) {
            this.process();
        }
    };
}

export default ProcessImages;
