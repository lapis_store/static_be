let count = 0;

const id = () => {
    const time = Date.now().toString(16);
    const n = count.toString(16).padStart(5, '0');
    count++;
    return time + n;
};

export default id;
