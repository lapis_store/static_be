import id from './id';

const make = {
    id,
};

export default make;
