import fs from 'node:fs';
import path from 'node:path';
import ENV from '../core/ENV';
import lapisLog from '../core/lapisLog';

export const removeFile = async (filePath: string) => {
    lapisLog('WARNING', filePath);
    if (!fs.existsSync(filePath)) return true;

    try {
        fs.unlinkSync(filePath);
        return true;
    } catch {
        return false;
    }
};

const removeImages = async (imageName: string) => {
    return !(
        await Promise.all([
            removeFile(path.join(ENV.IMAGES_LARGE_DIR, imageName)),
            removeFile(path.join(ENV.IMAGES_MEDIUM_DIR, imageName)),
            removeFile(path.join(ENV.IMAGES_SMALL_DIR, imageName)),
            removeFile(path.join(ENV.IMAGES_THUMBNAIL_DIR, imageName)),
        ])
    ).includes(false);
};

export default removeImages;
